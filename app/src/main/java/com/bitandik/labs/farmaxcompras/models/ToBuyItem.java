package com.bitandik.labs.farmaxcompras.models;
public class ToBuyItem {
  private String item;
  private String username;
  private boolean completed;

  public ToBuyItem(){ }

  public ToBuyItem(String item, String username) {
      this.username = username;
      this.item = item;
      this.completed = false;
  }
    public ToBuyItem(String item) {
        this.username = username;
        this.item = item;
        this.completed = false;
    }

    public String getUsername() {
      return username;
  }

  public void setUsername(String username) {
      this.username = username;
  }

  public String getItem() {
      return item;
  }

  public void setItem(String item) {
      this.item = item;
  }

  public boolean isCompleted() {
      return completed;
  }

  public void setCompleted(boolean completed) {
      this.completed = completed;
  }
}
