package com.bitandik.labs.farmaxcompras.models;

/**
 * Created by ERWIN on 29-Nov-17.
 */

public class ProductItem {
    private String productName;
    private double precio;
    private String farmacia;
    private String username;
    private boolean isAlreadyBuy;

    public ProductItem(){}
    public ProductItem(String productName, double precio, String farmacia, String username) {
        this.productName = productName;
        this.precio = precio;
        this.farmacia = farmacia;
        this.username = username;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getFarmacia() {
        return farmacia;
    }

    public void setFarmacia(String farmacia) {
        this.farmacia = farmacia;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isAlreadyBuy() {
        return isAlreadyBuy;
    }

    public void setAlreadyBuy(boolean isAlreadyBuy) {
        this.isAlreadyBuy = isAlreadyBuy;
    }
}
