package com.bitandik.labs.farmaxcompras.activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.bitandik.labs.farmaxcompras.R;
import com.bitandik.labs.farmaxcompras.RecordatorioApplication;
import com.bitandik.labs.farmaxcompras.adapters.RecordatorioItemsRecyclerAdapter;
import com.bitandik.labs.farmaxcompras.models.ProductItem;
import com.bitandik.labs.farmaxcompras.models.ToBuyItem;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RecordatorioMainActivity extends AppCompatActivity {
  @BindView(R.id.recycler_view_items) RecyclerView recyclerView;
  @BindView(R.id.editTextItem) EditText editTextItem;

  private DatabaseReference databaseReference;
  private FirebaseRecyclerAdapter adapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);
    setupUsername();
    SharedPreferences prefs = getApplication().getSharedPreferences("ToDoPrefs", 0);
    String username = prefs.getString("username", null);
    RecordatorioApplication app = (RecordatorioApplication)getApplicationContext();
    databaseReference = app.getItemsReference();
    /*ProductItem itemOne = new ProductItem("Panadol",35,"Carol",username);
    databaseReference.push().setValue(itemOne);

    ProductItem itemtwo = new ProductItem("Paracetamol",13,"Carol",username);
    databaseReference.push().setValue(itemtwo);

    ProductItem itemTres = new ProductItem("Ibuprofeno",40,"Carol",username);
    databaseReference.push().setValue(itemTres);

    ProductItem itemFour = new ProductItem("Frenadol",25,"Medicar GBC",username);
    databaseReference.push().setValue(itemFour);

    ProductItem itemFive = new ProductItem("Aspirina ",32,"Medicar GBC",username);
    databaseReference.push().setValue(itemFive);

    ProductItem itemSix = new ProductItem("Bisolgrip ",21,"Medicar GBC",username);
    databaseReference.push().setValue(itemSix);

    ProductItem item0 = new ProductItem("Calmagrip",13,"Hidalgos",username);
    databaseReference.push().setValue(item0);

    ProductItem itemFive0 = new ProductItem("Desenfriol ",17,"Hidalgos",username);
    databaseReference.push().setValue(itemFive0);

    ProductItem itemSix1 = new ProductItem("clorfenamina ",21,"Hidalgos",username);
    databaseReference.push().setValue(itemSix1);*/
   /* setupUsername();
    SharedPreferences prefs = getApplication().getSharedPreferences("ToDoPrefs", 0);
    String username = prefs.getString("username", null);
    setTitle("items para comprar: " + username);

   */
    adapter = new RecordatorioItemsRecyclerAdapter(R.layout.row, databaseReference);

    recyclerView.setLayoutManager(new LinearLayoutManager(this));
    recyclerView.setAdapter(adapter);
  }

  private void setupUsername() {
    SharedPreferences prefs = getApplication().getSharedPreferences("ToDoPrefs", 0);
    String username = prefs.getString("username", null);
    if (username == null) {
        Random r = new Random();
        username = "usuario" + r.nextInt(100000);
        prefs.edit().putString("username", username).commit();
    }
  }

  @OnClick(R.id.fab)
  public void addToDoItem() {
    SharedPreferences prefs = getApplication().getSharedPreferences("ToDoPrefs", 0);
    String username = prefs.getString("username", null);

    String itemText = editTextItem.getText().toString();
    editTextItem.setText("");

    InputMethodManager inputMethodManager = (InputMethodManager)  getSystemService(Activity.INPUT_METHOD_SERVICE);
    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

    if (!itemText.isEmpty()) {
        ToBuyItem toBuyItem = new ToBuyItem(itemText.trim(), username);
        databaseReference.push().setValue(toBuyItem);
    }
  }
}