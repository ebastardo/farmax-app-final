package com.bitandik.labs.farmaxcompras.adapters;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bitandik.labs.farmaxcompras.R;
import com.bitandik.labs.farmaxcompras.activities.RecordatorioMainActivity;
import com.bitandik.labs.farmaxcompras.activities.ViewItem;
import com.bitandik.labs.farmaxcompras.models.ProductItem;
import com.bitandik.labs.farmaxcompras.models.ToBuyItem;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecordatorioItemsRecyclerAdapter extends FirebaseRecyclerAdapter<ProductItem,
        RecordatorioItemsRecyclerAdapter.ToBuyItemViewHolder> {

  public RecordatorioItemsRecyclerAdapter(int modelLayout, DatabaseReference ref) {
    super(ProductItem.class, modelLayout, ToBuyItemViewHolder.class, ref);
  }

  @Override
  public ToBuyItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    ViewGroup view = (ViewGroup) LayoutInflater.from(parent.getContext()).inflate(mModelLayout, parent, false);
    return new ToBuyItemViewHolder(view);
  }

  @Override
  protected void populateViewHolder(ToBuyItemViewHolder holder, ProductItem item, int position) {
    holder.txtItem.setText(item.getProductName());
    holder.txtUser.setText("Precio: " +String.valueOf(item.getPrecio()));
    holder.txtFarmacia.setText("Farmacia " +item.getFarmacia());
   /* if (item.isCompleted()) {
      holder.imgDone.setVisibility(View.VISIBLE);
    } else {
      holder.imgDone.setVisibility(View.INVISIBLE);
    }*/
  }

  class ToBuyItemViewHolder extends RecyclerView.ViewHolder
                           implements View.OnClickListener,
                                      View.OnLongClickListener {

    @BindView(R.id.txtItem) TextView txtItem;
    @BindView(R.id.txtUser) TextView txtUser;
    @BindView(R.id.txtFarmacia) TextView txtFarmacia;
    @BindView(R.id.imgDone) ImageView imgDone;

    public ToBuyItemViewHolder(View itemView) {
      super(itemView);
      itemView.setOnClickListener(this);
      itemView.setOnLongClickListener(this);
      ButterKnife.bind(this, itemView);
    }



    @Override
    public void onClick(View view) {
     int position = getAdapterPosition();
      ProductItem currentItem = (ProductItem)getItem(position);
      DatabaseReference reference = getRef(position);
      Intent intent = new Intent(view.getContext(),ViewItem.class);
      intent.putExtra("farmacia",currentItem.getFarmacia());
      intent.putExtra("precio",currentItem.getPrecio());
      intent.putExtra("productName",currentItem.getProductName());

      view.getContext().startActivity(intent);

      System.out.println("oncliked view button");

     /* boolean completed = !currentItem.isCompleted();

      currentItem.setCompleted(completed);
      Map<String, Object> updates = new HashMap<String, Object>();
      updates.put("completed", completed);
      reference.updateChildren(updates);*/
    }

    @Override
    public boolean onLongClick(View view) {
      int position = getAdapterPosition();
      DatabaseReference reference = getRef(position);
      reference.removeValue();
      return true;
    }
  }
}

