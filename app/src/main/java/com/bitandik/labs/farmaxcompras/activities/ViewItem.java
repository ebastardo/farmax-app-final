package com.bitandik.labs.farmaxcompras.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.bitandik.labs.farmaxcompras.R;
import com.bitandik.labs.farmaxcompras.RecordatorioApplication;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewItem extends AppCompatActivity {


    TextView texfarmacia,textPrecio,textProductName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_item);
        ButterKnife.bind(this);
        setTitle("Farmaapp");
        texfarmacia=(TextView)findViewById(R.id.textFarmacia);
        textPrecio =(TextView)findViewById(R.id.textCantidad);
        textProductName = (TextView) findViewById(R.id.textProducto);
        Intent intent = getIntent();
        texfarmacia.setText(intent.getStringExtra("farmacia"));
        textPrecio.setText(String.valueOf(intent.getDoubleExtra("precio",0.0)));
        textProductName.setText(intent.getStringExtra("productName"));
    }
    @OnClick(R.id.idcomprar)
    public void buyItem(){
        Toast.makeText(this,"el item ha sido comprado con exito",Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, RecordatorioMainActivity.class);
        startActivity(intent);
    }
}
